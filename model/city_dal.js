var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM city;';

    connection.query(query, function(err, result) {
       callback(err, result);
    });
};

exports.getAreaCity = function(city_num, callback) {
    var query = 'CALL get_climbing_area_city(?);';

    var queryData = [city_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};