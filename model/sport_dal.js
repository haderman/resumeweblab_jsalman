var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM sport_lead;';

    connection.query(query, function(err, callback) {
        callback(err, result);
    });
};

exports.getById = function(s_num, callback) {
    var query = 'SELECT s.*, cr.crag_name, ca.area_name FROM sport_lead s ' +
        'LEFT JOIN crag cr ON cr.cr_num = s.cr_num ' +
        'LEFT JOIN climbing_area ca ON ca.ca_num = cr.ca_num ' +
        'WHERE s.s_num = ?;';

    var queryData = [s_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO sport_lead (cr_num, s_name, s_grade, s_rating, s_summary, num_bolts, s_risk) VALUES ?;';

    var queryData = [];

    queryData.push([params.cr_num, params.s_name, params.s_grade, params.s_rating, params.s_summary, params.num_bolts, params.s_risk]);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(s_num, callback) {
  var query = 'DELETE FROM sport_lead WHERE s_num = ?;';

  var queryData = [s_num];

  connection.query(query, queryData, function(err, result) {
      callback(err, result);
  });
};

exports.update = function(params, callback) {
    var query = 'Update sport_lead SET s_name = ?, s_grade = ?, s_rating = ?, s_summary = ?, num_bolts = ?, s_risk = ? WHERE s_num = ?;';

    var queryData = [params.s_name, params.s_grade, params.s_rating, params.s_summary, params.num_bolts, params.s_risk, params.s_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
