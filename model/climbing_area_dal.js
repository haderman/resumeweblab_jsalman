var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM climbing_area;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getSimple = function(ca_num, callback) {
    var query = 'SELECT ca.ca_num, ca.area_name FROM climbing_area ca ' +
        'WHERE ca.ca_num = ?;';
    console.log('ca_num: ' + ca_num);
    var queryData = [ca_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(ca_num, callback) {
    var query = 'SELECT ca.*, cr.cr_num, cr.crag_name, cr.cr_cli_type, ci.city_name FROM climbing_area ca ' +
        'LEFT JOIN crag cr ON cr.ca_num = ca.ca_num ' +
        'LEFT JOIN city ci ON ci.city_num = ca.city_num ' +
        'WHERE ca.ca_num = ?;';
    var queryData = [ca_num];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

/*
DROP PROCEDURE IF EXISTS get_route_rating_by_crag;
DELIMITER //
CREATE PROCEDURE get_route_rating_by_crag (_ca_num INT)
	BEGIN

    SELECT cr.cr_num, cr.crag_name, AVG(sl.s_rating) as sport_rating, AVG(b.b_rating) as boulder_rating, AVG(tl.t_rating) as trad_rating FROM crag cr
	LEFT JOIN sport_lead sl ON sl.cr_num = cr.cr_num
    LEFT JOIN boulder b ON b.cr_num = cr.cr_num
	LEFT JOIN trad_lead tl ON tl.cr_num = cr.cr_num
    LEFT JOIN climbing_area ca ON ca.ca_num = cr.ca_num
	where ca.ca_num = _ca_num
	group by cr.crag_name;

END//
DELIMITER ;
 */

exports.getCrag = function(ca_num, callback) {
    var query = 'CALL get_route_rating_by_crag(?);';

    var queryData = [ca_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getCity = function(ca_num, callback) {
    var query = 'SELECT ca.*, ci.* FROM climbing_area ca ' +
        'LEFT JOIN city ci ON ci.city_num = ca.city_num;';

    var queryData = [ca_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO climbing_area (city_num, area_name, approach_diff, ca_cli_type, address) VALUES ?;';

    var queryData = [];
    queryData.push([params.city_num, params.area_name, params.approach_diff, params.ca_cli_type, params.address]);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(ca_num, callback) {
    var query = 'DELETE FROM climbing_area WHERE ca_num = ?;';
    var queryData = [ca_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE climbing_area SET city_num = ?, area_name = ?, approach_diff = ?, ca_cli_type = ?, address = ? WHERE ca_num = ?;';
    var queryData = [params.city_num, params.area_name, params.approach_diff, params.ca_cli_type, params.address, params.ca_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
