var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM climber;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(clr_num, callback) {
    var query = 'SELECT clr.* FROM climber clr ' +
        'WHERE clr.clr_num = ?;';

    var queryData = [clr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getRouteCount = function(num, callback) {
    var query = 'SELECT clr.f_name, clr.l_name, COUNT(clr.clr_num) as number, clr.fav_climb FROM climber clr ' +
        'LEFT JOIN sport_climbs sc ON sc.clr_num = clr.clr_num ' +
        'LEFT JOIN boulder_climbs bc ON bc.clr_num = clr.clr_num ' +
        'LEFT JOIN trad_climbs tc ON tc.clr_num = clr.clr_num ' +
        'GROUP BY clr.clr_num ' +
        'HAVING number >= ?;';

    var queryData = [num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
exports.getSport = function(clr_num, callback) {
    var query = 'SELECT sl.s_num, sl.s_name, sl.s_grade FROM climber clr ' +
        'LEFT JOIN sport_climbs sc ON sc.clr_num = clr.clr_num ' +
        'LEFT JOIN sport_lead sl ON sl.s_num = sc.s_num ' +
        'WHERE clr.clr_num = ?;';

    var queryData = [clr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getBoulder = function(clr_num, callback) {
    var query = 'SELECT b.b_num, b.b_name, b.b_grade FROM climber clr ' +
        'LEFT JOIN boulder_climbs bc ON bc.clr_num = clr.clr_num ' +
        'LEFT JOIN boulder b ON b.b_num = bc.b_num ' +
        'WHERE clr.clr_num = ?;';

    var queryData = [clr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getTrad = function(clr_num, callback) {
    var query = 'SELECT tl.t_num, tl.t_name, tl.t_grade FROM climber clr ' +
        'LEFT JOIN trad_climbs tc ON tc.clr_num = clr.clr_num ' +
        'LEFT JOIN trad_lead tl ON tl.t_num = tc.t_num ' +
        'WHERE clr.clr_num = ?;';

    var queryData = [clr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE climber SET f_name = ?, l_name = ?, email = ?, fav_climb = ? WHERE clr_num = ?;';

    var queryData = [params.f_name, params.l_name, params.email, params.fav_climb, params.clr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback ) {
    var query = 'INSERT INTO climber (f_name, l_name, email, fav_climb ) VALUES ?;';

    var queryData = [];
    queryData.push([params.f_name, params.l_name, params.email, params.fav_climb]);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(clr_num, callback) {
    var query = 'DELETE FROM climber WHERE clr_num = ?;';

    var queryData = [clr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*
CREATE OR REPLACE VIEW get_all_routes AS
SELECT sl.s_name AS name, sl.s_grade AS grade FROM sport_lead sl
UNION
SELECT b.b_name, b.b_grade FROM boulder b
UNION
SELECT tl.t_name, tl.t_grade FROM trad_lead tl;
*/
exports.getAllRoutes = function(callback) {
    var query ='SELECT * FROM get_all_routes;';

    connection.query(query, function( err, result) {
        callback(err, result);
    });
};

exports.getSportAdd = function(clr_num, callback) {
    var query = 'SELECT sl.*, ca.area_name, cr.crag_name, sc.clr_num FROM sport_lead sl ' +
        'LEFT JOIN crag cr ON cr.cr_num = sl.cr_num ' +
        'LEFT JOIN climbing_area ca ON ca.ca_num = cr.ca_num ' +
        'LEFT JOIN sport_climbs sc ON sc.s_num = sl.s_num AND sc.clr_num = ?;';

    var queryData = [clr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getBoulderAdd = function(clr_num, callback) {
    var query = 'SELECT b.*, ca.area_name, cr.crag_name, bc.clr_num FROM boulder b ' +
        'LEFT JOIN crag cr ON cr.cr_num = b.cr_num ' +
        'LEFT JOIN climbing_area ca ON ca.ca_num = cr.ca_num ' +
        'LEFT JOIN boulder_climbs bc ON bc.b_num = b.b_num AND bc.clr_num = ?;';

    var queryData = [clr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getTradAdd = function(clr_num, callback) {
    var query = 'SELECT tl.*, ca.area_name, cr.crag_name, tc.clr_num FROM trad_lead tl ' +
        'LEFT JOIN crag cr ON cr.cr_num = tl.cr_num ' +
        'LEFT JOIN climbing_area ca ON ca.ca_num = cr.ca_num ' +
        'LEFT JOIN trad_climbs tc ON tc.t_num = tl.t_num AND tc.clr_num = ?;';

    var queryData = [clr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

//declare the function so it can be used locally
var sportClimbsInsert = function(clr_num, s_numArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var sportQuery = 'INSERT INTO sport_climbs (clr_num, s_num) VALUES ?;';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var sportQueryData = [];
    if (s_numArray.constructor === Array) {
        for (var i = 0; i < s_numArray.length; i++) {
            sportQueryData.push([clr_num, s_numArray[i]]);
        }
    }
    else {
        sportQueryData.push([clr_num, s_numArray]);
    }
    connection.query(sportQuery, [sportQueryData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.sportClimbsInsert = sportClimbsInsert;

//declare the function so it can be used locally
var sportClimbsDeleteAll = function(clr_num, callback){
    var query = 'DELETE FROM sport_climbs WHERE clr_num = ?;';
    var queryData = [clr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.sportClimbsDeleteAll = sportClimbsDeleteAll;

//declare the function so it can be used locally
var boulderClimbsInsert = function(clr_num, b_numArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var boulderQuery = 'INSERT INTO boulder_climbs (clr_num, b_num) VALUES ?;';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var boulderQueryData = [];
    if (b_numArray.constructor === Array) {
        for (var i = 0; i < b_numArray.length; i++) {
            boulderQueryData.push([clr_num, b_numArray[i]]);
        }
    }
    else {
        boulderQueryData.push([clr_num, b_numArray]);
    }
    connection.query(boulderQuery, [boulderQueryData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.boulderClimbsInsert = boulderClimbsInsert;

var boulderClimbsDeleteAll = function(clr_num, callback){
    var query = 'DELETE FROM boulder_climbs WHERE clr_num = ?;';
    var queryData = [clr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.boulderClimbsDeleteAll = boulderClimbsDeleteAll;

//declare the function so it can be used locally
var tradClimbsInsert = function(clr_num, t_numArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var tradQuery = 'INSERT INTO trad_climbs (clr_num, t_num) VALUES ?;';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var tradQueryData = [];
    if (t_numArray.constructor === Array) {
        for (var i = 0; i < t_numArray.length; i++) {
            tradQueryData.push([clr_num, t_numArray[i]]);
        }
    }
    else {
        tradQueryData.push([clr_num, t_numArray]);
    }
    connection.query(tradQuery, [tradQueryData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.tradClimbsInsert = tradClimbsInsert;

//declare the function so it can be used locally
var tradClimbsDeleteAll = function(clr_num, callback){
    var query = 'DELETE FROM trad_climbs WHERE clr_num = ?;';
    var queryData = [clr_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.tradClimbsDeleteAll = tradClimbsDeleteAll;

exports.insertClimbs = function(params, callback) {

    sportClimbsDeleteAll(params.clr_num, function(err, result){

        if(params.s_num != null) {
            sportClimbsInsert(params.clr_num, params.s_num, function (err, result) {
                boulderClimbsDeleteAll(params.clr_num, function (err, result) {

                    if (params.b_num != null) {
                        boulderClimbsInsert(params.clr_num, params.b_num, function (err, result) {
                            tradClimbsDeleteAll(params.clr_num, function (err, result) {

                                if (params.t_num != null) {
                                    tradClimbsInsert(params.clr_num, params.t_num, function (err, result) {
                                        callback(err, params.clr_num);
                                    });
                                }
                                else {
                                    callback(err, params.clr_num);
                                }
                            });
                        });
                    }
                    else {
                        tradClimbsDeleteAll(params.clr_num, function (err, result) {

                            if (params.t_num != null) {
                                tradClimbsInsert(params.clr_num, params.t_num, function (err, result) {
                                    callback(err, params.clr_num);
                                });
                            }
                            else {
                                callback(err, params.clr_num);
                            }
                        });
                    }
                });
            });
        }
        else {
            boulderClimbsDeleteAll(params.clr_num, function(err, result){

                if(params.b_num != null) {
                    boulderClimbsInsert(params.clr_num, params.b_num, function(err, result){
                        tracClimbsDeleteAll(params.clr_num, function(err, result){

                            if(params.t_num != null) {
                                tradClimbsInsert(params.clr_num, params.t_num, function(err, result){
                                    callback(err, params.clr_num);
                                });
                            }
                            else {
                                callback(err, params.clr_num);
                            }
                        });
                    });
                }
                else {
                    tradClimbsDeleteAll(params.clr_num, function(err, result){

                        if(params.t_num != null) {
                            tradClimbsInsert(params.clr_num, params.t_num, function(err, result){
                                callback(err, params.clr_num);
                            });
                        }
                        else {
                            callback(err, params.clr_num);
                        }
                    });
                }
            });
        }
    });
};
