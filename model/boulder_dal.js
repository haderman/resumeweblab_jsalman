var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM boulder;';

    connection.query(query, function(err, callback) {
        callback(err, result);
    });
};

exports.getById = function(b_num, callback) {
    var query = 'SELECT b.*, cr.crag_name, ca.area_name FROM boulder b ' +
        'LEFT JOIN crag cr ON cr.cr_num = b.cr_num ' +
        'LEFT JOIN climbing_area ca ON ca.ca_num = cr.ca_num ' +
        'WHERE b.b_num = ?;';

    var queryData = [b_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query =  'INSERT INTO boulder (cr_num, b_name, b_grade, b_rating, b_summary, boulder_name, b_risk) VALUES ?;';

    var queryData = [];

    queryData.push([params.cr_num, params.b_name, params.b_grade, params.b_rating, params.b_summary, params.boulder_name, params.b_risk]);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(b_num, callback) {
    var query = 'DELETE FROM boulder WHERE b_num = ?;';

    var queryData = [b_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'Update boulder SET b_name = ?, b_grade = ?, b_rating = ?, b_summary = ?, boulder_name = ?, b_risk = ? WHERE b_num = ?;';

    var queryData = [params.b_name, params.b_grade, params.b_rating, params.b_summary, params.boulder_name, params.b_risk, params.b_num];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
