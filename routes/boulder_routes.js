var express = require('express');
var router = express.Router();
var boulder_dal = require('../model/boulder_dal');
var crag_dal = require('../model/crag_dal');

router.get('/', function(req, res) {
    if (req.query.b_num == null) {
        res.send('b_num is null');
    } else {
        boulder_dal.getById(req.query.b_num, function(err, boulder) {
            if (err) {
                res.send(err);
            } else {
                res.render('boulder/boulderViewByID', { 'boulder' : boulder });
            }
        });
    }
});

router.get('/insert', function(req, res) {
    if (req.query.cr_num == null) {
        res.send('cr_num is null');
    } else if (req.query.b_name == null) {
        res.send('Please provide a climb name.');
    } else if (req.query.b_grade == null) {
        res.send('Please provide a climb grade');
    } else if (req.query.b_rating == null) {
        res.send('Please rate this climb');
    } else if (req.query.b_summary == null) {
        res.send('Please provide a summary of this climb');
    } else if (req.query.boulder_name == null) {
        res.send('Please provide the name of this boulder where this climb can be found');
    } else if (req.query.b_risk == null) {
        res.send('Please specify any additional risks, or enter "none" if there are none')
    } else
        boulder_dal.insert(req.query, function(err, result) {
            if (err) {
                res.send(err);
            } else {
                crag_dal.getById(req.query.cr_num, function(err, crag) {
                    crag_dal.getSport(req.query.cr_num, function(err, sport) {
                        crag_dal.getBoulder(req.query.cr_num, function(err, boulder) {
                            crag_dal.getTrad(req.query.cr_num, function(err, trad) {
                                if (err) {
                                    res.send(err);
                                }
                                else {
                                    res.render('crag/cragViewById', {'crag': crag, 'sport' : sport, 'boulder' : boulder, 'trad': trad });
                                }
                            });
                        });
                    });
                });
            }
        });
});

router.get('/edit', function(req, res) {
    if (req.query.b_num == null) {
        res.send('s_num is null');
    } else if (req.query.cr_num == null) {
        res.send('cr_num is null');
    } else {
        boulder_dal.getById(req.query.b_num, function(err, boulder) {
            if (err) {
                res.send(err);
            } else {
                res.render('boulder/boulderUpdate', {'boulder': boulder})
            }
        });
    }
});

router.get('/update', function(req, res) {
    boulder_dal.update(req.query, function(err, result) {
        if (err) {
            res.send(err);
        } else {
            boulder_dal.getById(req.query.b_num, function(err, boulder) {
                if (err) {
                    res.send(err);
                } else {
                    res.render('boulder/boulderViewById', {'boulder':boulder});
                }
            });
        }
    });
});

router.get('/delete', function(req, res) {
    boulder_dal.delete(req.query.b_num, function(err, result) {
        if (err) {
            res.send(err);
        } else {
            crag_dal.getById(req.query.cr_num, function(err, crag) {
                crag_dal.getSport(req.query.cr_num, function(err, sport) {
                    crag_dal.getBoulder(req.query.cr_num, function(err, boulder) {
                        crag_dal.getTrad(req.query.cr_num, function(err, trad) {
                            if (err) {
                                res.send(err);
                            }
                            else {
                                res.render('crag/cragViewById', {'crag': crag, 'sport' : sport, 'boulder' : boulder, 'trad': trad });
                            }
                        });
                    });
                });
            });
        }
    });
});

module.exports = router;
