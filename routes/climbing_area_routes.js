var express = require('express');
var router = express.Router();
var climbing_area_dal = require('../model/climbing_area_dal');
var city_dal = require('../model/city_dal');
//var address_dal = require('../model/address_dal');


// View All companys
router.get('/all', function(req, res) {
    climbing_area_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('climbing_area/climbing_areaViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.ca_num == null) {
        res.send('climbing area number is null');
    }
    else {
        climbing_area_dal.getById(req.query.ca_num, function(err, climbing_area) {
            climbing_area_dal.getCrag(req.query.ca_num, function(err, crag) {
                if (err) {
                    res.send(err);
                }
                else {
                    console.log(crag);
                    res.render('climbing_area/climbing_areaViewById', {'climbing_area': climbing_area, 'crag': crag[0] });
                }
            });
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    city_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('climbing_area/climbing_areaAdd', {'city': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.area_name == null) {
        res.send('Company Name must be provided.');
    }
    else if(req.query.approach_diff == null) {
        res.send('Please describe the approach difficulty');
    }
    else if(req.query.ca_cli_type == null) {
        res.send('Please specify the climbing types that can be found at this area');
    } else if(req.query.address == null) {
        res.send('Please include an address for this area, i.e. the location of the parking lot.')
    }
    else if (req.query.city_num == null) {
        res.send('A city must be selected')
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        climbing_area_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                climbing_area_dal.getById(result.insertId, function(err, result) {
                   res.render('climbing_area/climbing_areaViewById', {'result' : result });
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.ca_num == null) {
        res.send('A climbing area number must be provided');
    }
    else {
        climbing_area_dal.getById(req.query.ca_num, function(err, climbing_area){
            climbing_area_dal.getCity(req.query.ca_num, function(err, city) {
               if (err) {
                   res.send(err);
               } else {
                   res.render('climbing_area/climbing_areaUpdate', {'climbing_area' : climbing_area, 'city' : city});

               }
            });
        });
    }

});

router.get('/update', function(req, res) {
    climbing_area_dal.update(req.query, function(err, result){
        if (err) {
            res.send(err);
        } else {
            climbing_area_dal.getById(req.query.ca_num, function(err, result) {
                res.render('climbing_area/climbing_areaViewById', {'result' : result });
            });
        }
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.ca_num == null) {
        res.send('ca_num is null');
    }
    else {
        console.log('number to be deleted' + req.query.ca_num);
        climbing_area_dal.delete(req.query.ca_num, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                climbing_area_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('climbing_area/climbing_areaViewAll', { 'result':result });
                    }
                });
            }
        });
    }
});

module.exports = router;
