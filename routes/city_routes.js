var express = require('express');
var router = express.Router();
var city_dal = require('../model/city_dal');

router.get('/citySelect', function(req, res) {
    city_dal.getAll(function(err, city) {
        if (err) {
            res.send(err);
        } else {
            res.render('city/citySelectCity', {'city':city});
        }
    });
});

router.get('/citySelected', function(req, res) {
    if (req.query.city_num == null) {
        res.send('city_num is null');
    } else {
        city_dal.getAreaCity(req.query.city_num, function(err, result) {
            if (err) {
                res.send(err);
            } else {
                console.log(result);
                res.render('city/cityViewById', {'result':result[0]});
            }
        });
    }
});

module.exports = router;