var express = require('express');
var router = express.Router();
var trad_dal = require('../model/trad_dal');
var crag_dal = require('../model/crag_dal')

router.get('/', function(req, res) {
    if (req.query.t_num == null) {
        res.send('t_num is null');
    } else {
        trad_dal.getById(req.query.t_num, function(err, trad) {
            if (err) {
                res.send(err);
            } else {
                res.render('trad/tradViewByID', { 'trad' : trad });
            }
        });
    }
});

router.get('/insert', function(req, res) {
    if (req.query.cr_num == null) {
        res.send('cr_num is null');
    } else if (req.query.t_name == null) {
        res.send('Please provide a route name.');
    } else if (req.query.t_grade == null) {
        res.send('Please provide a route grade');
    } else if (req.query.t_rating == null) {
        res.send('Please rate thsi route');
    } else if (req.query.t_summary == null) {
        res.send('Please provide a summary of this route');
    } else if (req.query.descent == null) {
        res.send('Please specify the method of descent');
    } else if (req.query.safety == null) {
        res.send('Please provide a description of any permanent safety gear, if there are none, enter "none"')
    } else if (req.query.num_pitches == null) {
        res.send('Please specify the number of pitches on this route');
    } else if (req.query.t_risk == null) {
        res.send('Please specify any additional risks, or enter "none" if there are none')
    } else
        trad_dal.insert(req.query, function(err, result) {
            if (err) {
                res.send(err);
            } else {
                crag_dal.getById(req.query.cr_num, function(err, crag) {
                    crag_dal.getSport(req.query.cr_num, function(err, sport) {
                        crag_dal.getBoulder(req.query.cr_num, function(err, boulder) {
                            crag_dal.getTrad(req.query.cr_num, function(err, trad) {
                                if (err) {
                                    res.send(err);
                                }
                                else {
                                    res.render('crag/cragViewById', {'crag': crag, 'sport' : sport, 'boulder' : boulder, 'trad': trad });
                                }
                            });
                        });
                    });
                });
            }
        });
});

router.get('/edit', function(req, res) {
    if (req.query.t_num == null) {
        res.send('t_num is null');
    } else if (req.query.cr_num == null) {
        res.send('cr_num is null');
    } else {
        trad_dal.getById(req.query.t_num, function(err, trad) {
            if (err) {
                res.send(err);
            } else {
                res.render('trad/tradUpdate', {'trad': trad})
            }
        });
    }
});

router.get('/update', function(req, res) {
    trad_dal.update(req.query, function(err, result) {
        if (err) {
            res.send(err);
        } else {
            trad_dal.getById(req.query.t_num, function (err, trad) {
                if (err) {
                    res.send(err);
                } else {
                    res.render('trad/tradViewById', {'trad': trad});
                }
            });
        }
    });
});

router.get('/delete', function(req, res) {
    trad_dal.delete(req.query.t_num, function(err, result) {
        if (err) {
            res.send(err);
        } else {
            crag_dal.getById(req.query.cr_num, function(err, crag) {
                crag_dal.getSport(req.query.cr_num, function(err, sport) {
                    crag_dal.getBoulder(req.query.cr_num, function(err, boulder) {
                        crag_dal.getTrad(req.query.cr_num, function(err, trad) {
                            if (err) {
                                res.send(err);
                            }
                            else {
                                res.render('crag/cragViewById', {'crag': crag, 'sport' : sport, 'boulder' : boulder, 'trad': trad });
                            }
                        });
                    });
                });
            });
        }
    });
});

module.exports = router;
