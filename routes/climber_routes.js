var express = require('express');
var router = express.Router();
var climber_dal = require('../model/climber_dal');
var city_dal = require('../model/city_dal');
var sport_dal = require('../model/sport_dal');
var boulder_dal = require('../model/boulder_dal');
var trad_dal = require('../model/trad_dal');

router.get('/selectClimber', function(req, res) {
   climber_dal.getAll(function(err, climber) {
       if (err) {
           res.send(err);
       } else {
           res.render('climber/climberSelectClimber', {'climber':climber});
       }
   });
});

router.get('/seeOther', function(req, res) {
    climber_dal.getRouteCount(req.query.num, function(err, climbers) {
        if (err) {
            res.send(err);
        } else {
            res.render('climber/climberViewOther', {'climbers' : climbers, clr_num: req.query.clr_num })
        }
    });
});

router.get('/climberSelected', function(req, res) {
    climber_dal.getById(req.query.clr_num, function(err, climber) {
        climber_dal.getBoulder(req.query.clr_num, function(err, boulder) {
            climber_dal.getSport(req.query.clr_num, function(err, sport) {
                climber_dal.getTrad(req.query.clr_num, function(err, trad) {
                    if (err) {
                        res.send(err);
                    } else {
                        res.render('climber/climberViewById', {'climber':climber, 'boulder':boulder, 'sport':sport, 'trad': trad })
                    }
                });
            });
        });
    });
});

router.get('/selectSport', function(req, res) {
    climber_dal.getById(req.query.clr_num, function(err, climber) {
        sport_dal.getById(req.query.s_num, function(err, sport) {
            if (err) {
                res.send(err);
            } else {
                res.render('climber/climberViewSport', { 'sport' : sport, 'climber' : climber });
            }
        });
    });
});

router.get('/selectBoulder', function(req, res) {
    climber_dal.getById(req.query.clr_num, function(err, climber) {
        boulder_dal.getById(req.query.b_num, function(err, boulder) {
            if (err) {
                res.send(err);
            } else {
                res.render('climber/climberViewBoulder', { 'boulder' : boulder, 'climber' : climber });
            }
        });
    });
});

router.get('/selectTrad', function(req, res) {
    climber_dal.getById(req.query.clr_num, function(err, climber) {
        trad_dal.getById(req.query.t_num, function(err, trad) {
            if (err) {
                res.send(err);
            } else {
                res.render('climber/climberViewTrad', { 'trad' : trad, 'climber' : climber });
            }
        });
    });
});


router.get('/insert', function(req, res) {
    if (req.query.f_name == null) {
        res.send('Please provide a first name.');
    } else if (req.query.l_name == null) {
        res.send('Please provide a last name.');
    } else if (req.query.email == null) {
        res.send('Please provide an email address.')
    } else if (req.query.fav_climb == null) {
        res.send('Please select your favorite climb.')
    } else {
        climber_dal.insert(req.query, function(err, result) {
            if (err) {
                res.send(err);
            } else {
                var clr_num = result.insertId;
                climber_dal.getById(clr_num, function(err, climber) {
                    climber_dal.getBoulder(clr_num, function(err, boulder) {
                        climber_dal.getSport(clr_num, function(err, sport) {
                            climber_dal.getTrad(clr_num, function(err, trad) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    res.render('climber/climberViewById', {'climber':climber, 'boulder':boulder, 'sport':sport, 'trad': trad, new_climber:true })
                                }
                            });
                        });
                    });
                });
            }
        });
    }
});

router.get('/delete', function(req, res) {
    climber_dal.delete(req.query.clr_num, function(err, result) {
        if (err) {
            res.send(err);
        } else {
            res.render('index', { title: 'Crag Tracker' });
        }
    });
});

router.get('/edit', function(req, res) {
    if (req.query.clr_num == null) {
        res.send('clr_num is null');
    } else {
        climber_dal.getById(req.query.clr_num, function(err, climber) {
            climber_dal.getAllRoutes(function(err, routes) {
                if(err) {
                    res.send(err);
                } else {
                    res.render('climber/climberUpdate', {'climber':climber, 'routes':routes });
                }
            });
        });
    }
});

router.get('/update', function(req, res) {
    console.log('router called correctly')
    if (req.query.f_name == null) {
        res.send('Please provide a first name.');
    } else if (req.query.l_name == null) {
        res.send('Please provide a last name.');
    } else if (req.query.email == null) {
        res.send('Please provide an email address.')
    } else if (req.query.fav_climb == null) {
        res.send('Please select your favorite climb.')
    } else {
        climber_dal.update(req.query, function (err, result) {
            if (err) {
                res.send(err);
            } else {
                var clr_num = req.query.clr_num;
                climber_dal.getById(clr_num, function (err, climber) {
                    climber_dal.getBoulder(clr_num, function (err, boulder) {
                        climber_dal.getSport(clr_num, function (err, sport) {
                            climber_dal.getTrad(clr_num, function (err, trad) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    res.render('climber/climberViewById', {
                                        'climber': climber,
                                        'boulder': boulder,
                                        'sport': sport,
                                        'trad': trad,
                                        update_successful: true
                                    });
                                }
                            });
                        });
                    });
                });
            }
        });
    }
});

router.get('/addNew', function(req, res) {
    climber_dal.getAllRoutes(function(err, routes) {
        if(err) {
            res.send(err);
        } else {
            res.render('climber/climberAddNew', { 'routes':routes });
        }
    });
});

router.get('/climbAdd', function(req, res) {
    climber_dal.insertClimbs(req.query, function(err, result) {
        if (err) {
            res.send(err);
        } else {
            var clr_num = req.query.clr_num;
            climber_dal.getById(clr_num, function (err, climber) {
                climber_dal.getBoulder(clr_num, function (err, boulder) {
                    climber_dal.getSport(clr_num, function (err, sport) {
                        climber_dal.getTrad(clr_num, function (err, trad) {
                            if (err) {
                                res.send(err);
                            } else {
                                res.render('climber/climberViewById', {
                                    'climber': climber,
                                    'boulder': boulder,
                                    'sport': sport,
                                    'trad': trad,
                                    routes_successful: true
                                });
                            }
                        });
                    });
                });
            });
        }
    });
});

router.get('/addClimbs', function(req, res) {
    if (req.query.clr_num == null) {
        res.send('clr_num is null');
    } else {
        var clr_num = req.query.clr_num;
        climber_dal.getById(clr_num, function(err, climber) {
            climber_dal.getSportAdd(clr_num, function(err, sport) {
                climber_dal.getBoulderAdd(clr_num, function(err, boulder) {
                    climber_dal.getTradAdd(clr_num, function(err, trad) {
                        console.log(climber);
                        console.log(sport);
                        console.log(boulder);
                        console.log(trad);
                        if(err) {
                            res.send(err);
                        } else {
                            res.render('climber/climberAddClimbs', {'climber':climber, 'sport':sport, 'boulder':boulder, 'trad':trad});
                        }
                    });
                });
            });
        });
    }
});

module.exports = router;
