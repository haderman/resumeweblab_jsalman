var express = require('express');
var router = express.Router();
var sport_dal = require('../model/sport_dal');
var crag_dal = require('../model/crag_dal');

router.get('/', function(req, res) {
    if (req.query.s_num == null) {
        res.send('s_num is null');
    } else {
        sport_dal.getById(req.query.s_num, function(err, sport) {
            if (err) {
                res.send(err);
            } else {
                res.render('sport/sportViewByID', { 'sport' : sport });
            }
        });
    }
});

router.get('/insert', function(req, res) {
    if (req.query.cr_num == null) {
        res.send('cr_num is null');
    } else if (req.query.s_name == null) {
        res.send('Please provide a route name.');
    } else if (req.query.s_grade == null) {
        res.send('Please provide a route grade');
    } else if (req.query.s_rating == null) {
        res.send('Please rate thsi route');
    } else if (req.query.s_summary == null) {
        res.send('Please provide a summary of this route');
    } else if (req.query.num_bolts == null) {
        res.send('Please specify the number of bolts on this route');
    } else if (req.query.s_risk == null) {
        res.send('Please specify any additional risks, or enter "none" if there are none')
    } else
        sport_dal.insert(req.query, function(err, result) {
            if (err) {
                res.send(err);
            } else {
                crag_dal.getById(req.query.cr_num, function(err, crag) {
                    crag_dal.getSport(req.query.cr_num, function(err, sport) {
                        crag_dal.getBoulder(req.query.cr_num, function(err, boulder) {
                            crag_dal.getTrad(req.query.cr_num, function(err, trad) {
                                if (err) {
                                    res.send(err);
                                }
                                else {
                                    res.render('crag/cragViewById', {'crag': crag, 'sport' : sport, 'boulder' : boulder, 'trad': trad });
                                }
                            });
                        });
                    });
                });
            }
        });
});

router.get('/edit', function(req, res) {
    if (req.query.s_num == null) {
        res.send('s_num is null');
    } else if (req.query.cr_num == null) {
        res.send('cr_num is null');
    } else {
        sport_dal.getById(req.query.s_num, function(err, sport) {
           if (err) {
               res.send(err);
           } else {
               res.render('sport/sportUpdate', {'sport': sport})
           }
        });
    }
});

router.get('/update', function(req, res) {
    sport_dal.update(req.query, function(err, result) {
        if (err) {
            res.send(err);
        } else {
            sport_dal.getById(req.query.s_num, function(err, sport) {
                if (err) {
                    res.send(err);
                } else {
                    res.render('sport/sportViewById', {'sport':sport});
                }
            });
        }
    });
});

router.get('/delete', function(req, res) {
    sport_dal.delete(req.query.s_num, function(err, result) {
        if (err) {
            res.send(err);
        } else {
            crag_dal.getById(req.query.cr_num, function(err, crag) {
                crag_dal.getSport(req.query.cr_num, function(err, sport) {
                    crag_dal.getBoulder(req.query.cr_num, function(err, boulder) {
                        crag_dal.getTrad(req.query.cr_num, function(err, trad) {
                            if (err) {
                                res.send(err);
                            }
                            else {
                                res.render('crag/cragViewById', {'crag': crag, 'sport' : sport, 'boulder' : boulder, 'trad': trad });
                            }
                        });
                    });
                });
            });
        }
    });
});

module.exports = router;
