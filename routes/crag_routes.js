var express = require('express');
var router = express.Router();
var crag_dal = require('../model/crag_dal');
var climbing_area_dal  = require('../model/climbing_area_dal');
//var address_dal = require('../model/address_dal');


// View All companys
router.get('/all', function(req, res) {
    crag_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('crag/cragViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.cr_num == null) {
        res.send('crag number is null');
    }
    else {
        crag_dal.getById(req.query.cr_num, function(err, crag) {
            crag_dal.getSport(req.query.cr_num, function(err, sport) {
                crag_dal.getBoulder(req.query.cr_num, function(err, boulder) {
                    crag_dal.getTrad(req.query.cr_num, function(err, trad) {
                        if (err) {
                            res.send(err);
                        }
                        else {
                            res.render('crag/cragViewById', {'crag': crag, 'sport' : sport, 'boulder' : boulder, 'trad': trad });
                        }
                    });
                });
            });
        });
    }
});

router.get('/selectRoute', function(req, res) {
    if (req.query.cr_num == null) {
        res.send('cr_num is null');
    } else {
        crag_dal.getSimple(req.query.cr_num, function(err, result) {
            if (err) {
                res.send(err);
            } else {
                res.render('crag/cragAddRoute', {'result' : result });
            }
        });
    }
});

router.get('/addRoute', function(req, res) {
    if (req.query.cr_num == null) {
        res.send('cr_num is null');
    } else {
        crag_dal.getSimple(req.query.cr_num, function(err, result) {
            if (err) {
                res.send(err);
            } else {
                if (req.query.route_type == 1) {
                    res.render('sport/sportAdd', {'crag' : result });
                } else if (req.query.route_type == 2) {
                    res.render('boulder/boulderAdd', {'crag' : result });
                }  else if (req.query.route_type == 3) {
                    res.render('trad/tradAdd', { 'crag' : result });
                } else {
                    res.send('Please select a climbing type');
                }
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    if (req.query.ca_num == null) {
        res.send('ca_num is null');
    } else {
        climbing_area_dal.getSimple(req.query.ca_num, function(err, result) {
            if (err) {
                res.send(err);
            } else {
                console.log(result);
                res.render('crag/cragAdd', { 'climbing_area' : result });
            }
        });
    }
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.crag_name == null) {
        res.send('Company Name must be provided.');
    }
    else if(req.query.location == null) {
        res.send('Please provide the location of the crag within the climbing area');
    }
    else if(req.query.cr_cli_type == null) {
        res.send('Please specify the climbing types that can be found at this crag');
    } else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        crag_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                var cr_num = result.insertId;
                console.log('New crag ID: ' + cr_num);
                crag_dal.getById(cr_num, function(err, crag) {
                    crag_dal.getSport(cr_num, function(err, sport) {
                        crag_dal.getBoulder(cr_num, function(err, boulder) {
                            crag_dal.getTrad(cr_num, function(err, trad) {
                                if (err) {
                                    res.send(err);
                                }
                                else {
                                    res.render('crag/cragViewById', {'crag': crag, 'sport' : sport, 'boulder' : boulder, 'trad': trad });
                                }
                            });
                        });
                    });
                });
            }
        });
    }
});



router.get('/edit', function(req, res){
    if(req.query.cr_num == null) {
        res.send('cr_num is null');
    }
    else {
        crag_dal.getById(req.query.cr_num, function(err, crag){
                if (err) {
                    res.send(err);
                } else {
                    res.render('crag/cragUpdate', {'crag' : crag});
                }
        });
    }

});

router.get('/update', function(req, res) {
    crag_dal.update(req.query, function(err, result){
        if (err) {
            res.send(err);
        } else {
            crag_dal.getById(req.query.cr_num, function(err, result) {
                crag_dal.getById(req.query.cr_num, function (err, crag) {
                    crag_dal.getSport(req.query.cr_num, function (err, sport) {
                        crag_dal.getBoulder(req.query.cr_num, function (err, boulder) {
                            crag_dal.getTrad(req.query.cr_num, function (err, trad) {
                                if (err) {
                                    res.send(err);
                                }
                                else {
                                    res.render('crag/cragViewById', {
                                        'crag': crag,
                                        'sport': sport,
                                        'boulder': boulder,
                                        'trad': trad
                                    });
                                }
                            });
                        });
                    });
                });
            });
        }
    });
});


// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.cr_num == null) {
        res.send('ca_num is null');
    }
    else {
        console.log('number to be deleted' + req.query.cr_num);
        crag_dal.delete(req.query.cr_num, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
              climbing_area_dal.getById(req.query.ca_num, function(err, climbing_area) {
                    climbing_area_dal.getCrag(req.query.ca_num, function(err, crag) {
                         if (err) {
                            res.send(err);
                        }
                        else {
                            console.log(crag);
                            res.render('climbing_area/climbing_areaViewById', {'climbing_area': climbing_area, 'crag': crag[0] });
                        }
                    });
                });
            }
        });
    }
});

module.exports = router;
