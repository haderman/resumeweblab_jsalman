var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var climbing_area = require('./routes/climbing_area_routes');
var crag = require('./routes/crag_routes');
var sport = require('./routes/sport_routes');
var boulder = require('./routes/boulder_routes');
var trad = require('./routes/trad_routes');
var climber = require('./routes/climber_routes');
var city = require('./routes/city_routes');
var about = require('./routes/about_routes');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/climbing_area', climbing_area);
app.use('/crag', crag);
app.use('/sport', sport);
app.use('/trad', trad);
app.use('/boulder', boulder);
app.use('/climber', climber);
app.use('/about', about);
app.use('/city', city);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
